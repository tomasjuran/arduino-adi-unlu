int buttons[] = {8, 9, 10, 11};
int buttonsLength = sizeof(buttons) / sizeof(int);
int leds[] = {4, 5, 6, 7};
int buzzer = 13;

const int gameLength = 10;
int sequence[gameLength];
int currentIndex = 0;

int state = 0;
int stage = 3;

void setup() {
  for (int i = 0; i < buttonsLength; i++) {
    pinMode(buttons[i], INPUT);
    pinMode(leds[i], OUTPUT);
    digitalWrite(leds[i], LOW);
  }
  pinMode(buzzer, OUTPUT);
  randomSeed(analogRead(0));
  playVictoryMusic();
  restart();
}

void restart() {
  generateSequence();
  currentIndex = 0;
  state = 0;
  stage = 3;
  for (int i = 0; i < buttonsLength; i++) {
    digitalWrite(leds[i], LOW);
  }
  delay(1000);
}

void generateSequence() {
  for (int i = 0; i < gameLength; i++) {
    sequence[i] = random(0, buttonsLength);
  }
}

void playSequence(int amount) {
  for (int i = 0; i < amount; i++) {
    play(sequence[i]);
  }
}

void playVictoryMusic() {
  for (int i = 0; i < 3; i++) {
    for (int k = 0; k < buttonsLength; k = k + 2) {
      digitalWrite(leds[k], HIGH);
    }
    for (int k = 1; k < buttonsLength; k = k + 2) {
      digitalWrite(leds[k], LOW);
    }
    tone(buzzer, 960, 250);
    delay(255);
    for (int k = 0; k < buttonsLength; k = k + 2) {
      digitalWrite(leds[k], LOW);
    }
    for (int k = 1; k < buttonsLength; k = k + 2) {
      digitalWrite(leds[k], HIGH);
    }
    tone(buzzer, 1080, 250);
    delay(255);
  }
}

void play(int index) {
  digitalWrite(leds[index], HIGH);
  tone(buzzer, 330 + index * 55, 500);
  delay(1000);
  digitalWrite(leds[index], LOW);
  delay(100);
}

void loop() {
  int input = -1;
  int buttonIndex = 0;

  if (state == 0) {
    playSequence(stage);
    state = 1;
  } else {
    while ((buttonIndex < buttonsLength) and (input == -1)) {
      if (digitalRead(buttons[buttonIndex]) == HIGH) {
        input = buttonIndex;
        play(input);
      }
      buttonIndex = buttonIndex + 1;
    }

    if (input > -1) {
      if (sequence[currentIndex] == input) {
        // Right
        currentIndex = currentIndex + 1;
        if (currentIndex >= gameLength) {
          // Won
          playVictoryMusic();
          restart();
        } else {
          if (currentIndex >= stage) {
            // Next stage
            stage = stage + 1;
            currentIndex = 0;
            state = 0;
            delay(1000);
          }
        }
      } else {
        // Wrong
        tone(buzzer, 220, 1000);
        delay(1500);
        for (int a = 0; a < 3; a++) {
          play(sequence[currentIndex]);
        }
        restart();
      }
    }
  }
}
