int pinRed = 9;
int pinYel = 8;
int pinGre = 7;
void setup() {
  pinMode(pinRed, OUTPUT);
  pinMode(pinYel, OUTPUT);
  pinMode(pinGre, OUTPUT);
  digitalWrite(pinRed,LOW);
  digitalWrite(pinYel,LOW);
  digitalWrite(pinGre,LOW);
}
void loop() {
  digitalWrite(pinRed,HIGH);
  delay(3000);
  digitalWrite(pinYel,HIGH);
  delay(1000);
  digitalWrite(pinRed,LOW);
  digitalWrite(pinYel,LOW);
  digitalWrite(pinGre,HIGH);
  delay(3000);
  digitalWrite(pinGre,LOW);
  digitalWrite(pinYel,HIGH);
  delay(1000);
  digitalWrite(pinYel,LOW);
}
